#include "mbed.h"
#include "LSM6DS3.h"
#include "XBeeLib.h"
#if defined(ENABLE_LOGGING)
#include "DigiLoggerMbedSerial.h"
using namespace DigiLog;
#endif

#define REMOTE_NODE_ADDR64_MSB  ((uint32_t)0x0013A200)

#define REMOTE_NODE_ADDR64_LSB  ((uint32_t)0x403E0961)

#define REMOTE_NODE_ADDR64      UINT64(REMOTE_NODE_ADDR64_MSB, REMOTE_NODE_ADDR64_LSB)

using namespace XBeeLib;

Serial *log_serial;

LSM6DS3 LSM6DS3(p28,p27);
DigitalOut myled1(LED1);
DigitalOut myled2(LED2);
DigitalOut myled3(LED3);
DigitalOut myled4(LED4);
Serial pc(USBTX, USBRX); // tx, rx

Serial xbee(p13,p14);
DigitalOut rst(p8);

Timer t;

static void send_data_to_coordinator(XBeeZB& xbee,const char data[])
{
    //const char data[] = "3145678";
    const uint16_t data_len = strlen(data);

    const TxStatus txStatus = xbee.send_data_to_coordinator((const uint8_t *)data, data_len);
    if (txStatus == TxStatusSuccess)
        log_serial->printf("send_data_to_coordinator OK\r\n");
    else
        log_serial->printf("send_data_to_coordinator failed with %d\r\n", (int) txStatus);
}

int main() {
	log_serial = new Serial(DEBUG_TX, DEBUG_RX);
    log_serial->baud(9600);

	#if defined(ENABLE_LOGGING)
    new DigiLoggerMbedSerial(log_serial, LogLevelInfo);
	#endif

    XBeeZB xbee = XBeeZB(RADIO_TX, RADIO_RX, RADIO_RESET, NC, NC, 9600);

    RadioStatus radioStatus = xbee.init();
    MBED_ASSERT(radioStatus == Success);

    /* Wait until the device has joined the network */
    log_serial->printf("Waiting for device to join the network: ");
    while (!xbee.is_joined()) {
        wait_ms(1000);
        log_serial->printf(".");
    }
    log_serial->printf("OK\r\n");

    const RemoteXBeeZB remoteDevice = RemoteXBeeZB(REMOTE_NODE_ADDR64);
	
	set_time(1256729737);  // Set RTC time to Wed, 28 Oct 2009 11:35:37
    LSM6DS3.begin();
	
	uint32_t current_time = 0;
	uint32_t previous_time = 0;
	float delta_time=0;

	char direction[]="0";
	float current_angle=0;
	float previous_angle=0;
	int counter = 0;

	t.start();
	myled1 = 1;
	myled2 = 1;
	myled3 = 1;
	myled4 = 1;
    while(1)
    {

        //read Accel & Gyro
        //LSM6DS3.readAccel();
        LSM6DS3.readGyro();
        
        //serial send Accel
        //pc.printf("\nAccelerX[%f]\n",LSM6DS3.ax);
        //pc.printf("AccelerY[%f]\n",LSM6DS3.ay);
        //pc.printf("AccelerZ[%f]\n",LSM6DS3.az);
        
        //serial send Gyro
        //pc.printf("\nGyroX[%f]\n",LSM6DS3.gx);
        //pc.printf("GyroY[%f]\n",LSM6DS3.gy);
        //pc.printf("GyroZ[%f]\n",LSM6DS3.gz);
			
		current_time = t.read_ms();			
		delta_time = (current_time - previous_time)/(1000.0f);
		float value = LSM6DS3.gz;
		if(value < 15 && value > -15){
			value = 0;
		}					
		
		float delta_angle = value * delta_time;
		previous_angle = current_angle;
		current_angle = current_angle + delta_angle;
		if(current_angle == previous_angle){
			counter++;
			if(counter==3)
			{
				if(current_angle > -45.0f && current_angle <= 45.0f)
				{
					current_angle = 0.0f;
					direction[0] = '0';
					myled1 = 1;
					myled2 = 0;
					myled3 = 0;
					myled4 = 0;
				}
				if(current_angle <= -45.0f && current_angle > -135.0f)
				{
					current_angle = -90.0f;
					direction[0] = '3';
					myled1 = 0;
					myled2 = 0;
					myled3 = 0;
					myled4 = 1;
				}
				if(current_angle > 45.0f && current_angle <= 135.0f)
				{
					current_angle = 90.0f;
					direction[0] = '1';
					myled1 = 0;
					myled2 = 1;
					myled3 = 0;
					myled4 = 0;
				}
				if(current_angle > 135.0f && current_angle <= 225.0f)
				{
					current_angle = 180.0f;
					direction[0] = '2';
					myled1 = 0;
					myled2 = 0;
					myled3 = 1;
					myled4 = 0;
				}
				if(current_angle <= -135.0f && current_angle > -225.0f)
				{
					current_angle = 180.0f;
					direction[0] = '2';
					myled1 = 0;
					myled2 = 0;
					myled3 = 1;
					myled4 = 0;
				}
				if(current_angle > 225.0f && current_angle <= 315.0f)
				{
					current_angle = -90.0f;
					direction[0] = '3';
					myled1 = 0;
					myled2 = 0;
					myled3 = 0;
					myled4 = 1;
				}
				counter=0;
			}
		}
		else{
			counter=0;
		}
		
		previous_time = current_time;
		
		
		//printf("delta_time = %f\n", delta_time);
		printf("Angle = %f\n", current_angle);
		//pc.printf("GyroZ[%f]\n\n",LSM6DS3.gz);
		//pc.printf("Value[%f]\n",value);
		
		send_data_to_coordinator(xbee,direction);
						
		wait_ms(200);
    }
   
}
